/* generated vector header file - do not edit */
#ifndef VECTOR_DATA_H
#define VECTOR_DATA_H
/* Number of interrupts allocated */
#ifndef VECTOR_DATA_IRQ_COUNT
#define VECTOR_DATA_IRQ_COUNT    (4)
#endif
/* ISR prototypes */
void gpt_counter_overflow_isr(void);
void gpt_capture_a_isr(void);
void r_icu_isr(void);

/* Vector table allocations */
#define VECTOR_NUMBER_GPT0_CAPTURE_COMPARE_A ((IRQn_Type) 0)
#define VECTOR_NUMBER_ICU_IRQ0 ((IRQn_Type) 1) /* ICU IRQ0 (External pin interrupt 0) */
#define VECTOR_NUMBER_GPT1_COUNTER_OVERFLOW ((IRQn_Type) 2) /* GPT1 COUNTER OVERFLOW (Overflow) */
#define VECTOR_NUMBER_GPT2_COUNTER_OVERFLOW ((IRQn_Type) 3) /* GPT2 COUNTER OVERFLOW (Overflow) */
#define VECTOR_NUMBER_GPT0_COUNTER_OVERFLOW ((IRQn_Type) 4)
typedef enum IRQn
{
    Reset_IRQn = -15,
    NonMaskableInt_IRQn = -14,
    HardFault_IRQn = -13,
    MemoryManagement_IRQn = -12,
    BusFault_IRQn = -11,
    UsageFault_IRQn = -10,
    SVCall_IRQn = -5,
    DebugMonitor_IRQn = -4,
    PendSV_IRQn = -2,
    SysTick_IRQn = -1,
    GPPT0_CAPTURE_COMPARE_A_IRQn = 0,
    ICU_IRQ0_IRQn = 1, /* ICU IRQ0 (External pin interrupt 0) */
    GPT1_COUNTER_OVERFLOW_IRQn = 2, /* GPT1 COUNTER OVERFLOW (Overflow) */
    GPT2_COUNTER_OVERFLOW_IRQn = 3, /* GPT2 COUNTER OVERFLOW (Overflow) */
    GPT0_COUNTER_OVERFLOW_IRQn = 4,
} IRQn_Type;
#endif /* VECTOR_DATA_H */
