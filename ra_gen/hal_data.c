/* generated HAL source file - do not edit */
#include "hal_data.h"

dtc_instance_ctrl_t g_led_transfer_ctrl;

transfer_info_t g_led_transfer_info =
{ .dest_addr_mode = TRANSFER_ADDR_MODE_FIXED,
  .repeat_area = TRANSFER_REPEAT_AREA_SOURCE,
  .irq = TRANSFER_IRQ_END,
  .chain_mode = TRANSFER_CHAIN_MODE_DISABLED,
  .src_addr_mode = TRANSFER_ADDR_MODE_INCREMENTED,
  .size = TRANSFER_SIZE_4_BYTE,
  .mode = TRANSFER_MODE_NORMAL,
  .p_dest = (void*) NULL,
  .p_src = (void const*) NULL,
  .num_blocks = 0,
  .length = 0, };
const dtc_extended_cfg_t g_led_transfer_cfg_extend =
{ .activation_source = VECTOR_NUMBER_GPT0_CAPTURE_COMPARE_A, };
const transfer_cfg_t g_led_transfer_cfg =
{ .p_info = &g_led_transfer_info, .p_extend = &g_led_transfer_cfg_extend, };

/* Instance structure to use this module. */
const transfer_instance_t g_led_transfer =
{ .p_ctrl = &g_led_transfer_ctrl, .p_cfg = &g_led_transfer_cfg, .p_api = &g_transfer_on_dtc };
gpt_instance_ctrl_t g_timer_images_ctrl;
#if 0
const gpt_extended_pwm_cfg_t g_timer_images_pwm_extend =
{
    .trough_ipl          = (BSP_IRQ_DISABLED),
#if defined(VECTOR_NUMBER_GPT2_COUNTER_UNDERFLOW)
    .trough_irq          = VECTOR_NUMBER_GPT2_COUNTER_UNDERFLOW,
#else
    .trough_irq          = FSP_INVALID_VECTOR,
#endif
    .poeg_link           = GPT_POEG_LINK_POEG0,
    .output_disable      =  GPT_OUTPUT_DISABLE_NONE,
    .adc_trigger         =  GPT_ADC_TRIGGER_NONE,
    .dead_time_count_up  = 0,
    .dead_time_count_down = 0,
    .adc_a_compare_match = 0,
    .adc_b_compare_match = 0,
    .interrupt_skip_source = GPT_INTERRUPT_SKIP_SOURCE_NONE,
    .interrupt_skip_count  = GPT_INTERRUPT_SKIP_COUNT_0,
    .interrupt_skip_adc    = GPT_INTERRUPT_SKIP_ADC_NONE,
    .gtioca_disable_setting = GPT_GTIOC_DISABLE_PROHIBITED,
    .gtiocb_disable_setting = GPT_GTIOC_DISABLE_PROHIBITED,
};
#endif
const gpt_extended_cfg_t g_timer_images_extend =
        { .gtioca =
        { .output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW },
          .gtiocb =
          { .output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW },
          .shortest_pwm_signal = GPT_SHORTEST_LEVEL_OFF, .start_source = GPT_SOURCE_NONE, .stop_source = GPT_SOURCE_NONE, .clear_source =
                  GPT_SOURCE_NONE,
          .count_up_source = GPT_SOURCE_NONE, .count_down_source = GPT_SOURCE_NONE, .capture_a_source = GPT_SOURCE_NONE, .capture_b_source =
                  GPT_SOURCE_NONE,
          .capture_a_ipl = (BSP_IRQ_DISABLED), .capture_b_ipl = (BSP_IRQ_DISABLED),
#if defined(VECTOR_NUMBER_GPT2_CAPTURE_COMPARE_A)
    .capture_a_irq       = VECTOR_NUMBER_GPT2_CAPTURE_COMPARE_A,
#else
          .capture_a_irq = FSP_INVALID_VECTOR,
#endif
#if defined(VECTOR_NUMBER_GPT2_CAPTURE_COMPARE_B)
    .capture_b_irq       = VECTOR_NUMBER_GPT2_CAPTURE_COMPARE_B,
#else
          .capture_b_irq = FSP_INVALID_VECTOR,
#endif
          .capture_filter_gtioca = GPT_CAPTURE_FILTER_NONE,
          .capture_filter_gtiocb = GPT_CAPTURE_FILTER_NONE,
#if 0
    .p_pwm_cfg                   = &g_timer_images_pwm_extend,
#else
          .p_pwm_cfg = NULL,
#endif
        };
const timer_cfg_t g_timer_images_cfg =
{ .mode = TIMER_MODE_PERIODIC,
  /* Actual period: 0.1 seconds. Actual duty: 50%. */.period_counts = (uint32_t) 0xb71b00,
  .duty_cycle_counts = 0x5b8d80,
  .source_div = (timer_source_div_t) 0,
  .channel = 2,
  .p_callback = image_callback,
  .p_context = NULL,
  .p_extend = &g_timer_images_extend,
  .cycle_end_ipl = (3),
#if defined(VECTOR_NUMBER_GPT2_COUNTER_OVERFLOW)
    .cycle_end_irq       = VECTOR_NUMBER_GPT2_COUNTER_OVERFLOW,
#else
  .cycle_end_irq = FSP_INVALID_VECTOR,
#endif
        };
/* Instance structure to use this module. */
const timer_instance_t g_timer_images =
{ .p_ctrl = &g_timer_images_ctrl, .p_cfg = &g_timer_images_cfg, .p_api = &g_timer_on_gpt };
gpt_instance_ctrl_t g_timer_row_ctrl;
#if 0
const gpt_extended_pwm_cfg_t g_timer_row_pwm_extend =
{
    .trough_ipl          = (BSP_IRQ_DISABLED),
#if defined(VECTOR_NUMBER_GPT1_COUNTER_UNDERFLOW)
    .trough_irq          = VECTOR_NUMBER_GPT1_COUNTER_UNDERFLOW,
#else
    .trough_irq          = FSP_INVALID_VECTOR,
#endif
    .poeg_link           = GPT_POEG_LINK_POEG0,
    .output_disable      =  GPT_OUTPUT_DISABLE_NONE,
    .adc_trigger         =  GPT_ADC_TRIGGER_NONE,
    .dead_time_count_up  = 0,
    .dead_time_count_down = 0,
    .adc_a_compare_match = 0,
    .adc_b_compare_match = 0,
    .interrupt_skip_source = GPT_INTERRUPT_SKIP_SOURCE_NONE,
    .interrupt_skip_count  = GPT_INTERRUPT_SKIP_COUNT_0,
    .interrupt_skip_adc    = GPT_INTERRUPT_SKIP_ADC_NONE,
    .gtioca_disable_setting = GPT_GTIOC_DISABLE_PROHIBITED,
    .gtiocb_disable_setting = GPT_GTIOC_DISABLE_PROHIBITED,
};
#endif
const gpt_extended_cfg_t g_timer_row_extend =
        { .gtioca =
        { .output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW },
          .gtiocb =
          { .output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW },
          .shortest_pwm_signal = GPT_SHORTEST_LEVEL_OFF, .start_source = GPT_SOURCE_NONE, .stop_source = GPT_SOURCE_NONE, .clear_source =
                  GPT_SOURCE_NONE,
          .count_up_source = GPT_SOURCE_NONE, .count_down_source = GPT_SOURCE_NONE, .capture_a_source = GPT_SOURCE_NONE, .capture_b_source =
                  GPT_SOURCE_NONE,
          .capture_a_ipl = (BSP_IRQ_DISABLED), .capture_b_ipl = (BSP_IRQ_DISABLED),
#if defined(VECTOR_NUMBER_GPT1_CAPTURE_COMPARE_A)
    .capture_a_irq       = VECTOR_NUMBER_GPT1_CAPTURE_COMPARE_A,
#else
          .capture_a_irq = FSP_INVALID_VECTOR,
#endif
#if defined(VECTOR_NUMBER_GPT1_CAPTURE_COMPARE_B)
    .capture_b_irq       = VECTOR_NUMBER_GPT1_CAPTURE_COMPARE_B,
#else
          .capture_b_irq = FSP_INVALID_VECTOR,
#endif
          .capture_filter_gtioca = GPT_CAPTURE_FILTER_NONE,
          .capture_filter_gtiocb = GPT_CAPTURE_FILTER_NONE,
#if 0
    .p_pwm_cfg                   = &g_timer_row_pwm_extend,
#else
          .p_pwm_cfg = NULL,
#endif
        };
timer_cfg_t g_timer_row_cfg =
{ .mode = TIMER_MODE_PERIODIC,
  /* Actual period: 35.791394133333334 seconds. Actual duty: 50%. */.period_counts = (uint32_t) 0x100000000,
  .duty_cycle_counts = 0x80000000,
  .source_div = (timer_source_div_t) 0,
  .channel = 1,
  .p_callback = NULL,
  .p_context = NULL,
  .p_extend = &g_timer_row_extend,
  .cycle_end_ipl = (2),
#if defined(VECTOR_NUMBER_GPT1_COUNTER_OVERFLOW)
    .cycle_end_irq       = VECTOR_NUMBER_GPT1_COUNTER_OVERFLOW,
#else
  .cycle_end_irq = FSP_INVALID_VECTOR,
#endif
        };
/* Instance structure to use this module. */
const timer_instance_t g_timer_row =
{ .p_ctrl = &g_timer_row_ctrl, .p_cfg = &g_timer_row_cfg, .p_api = &g_timer_on_gpt };
icu_instance_ctrl_t g_external_irq0_ctrl;
const external_irq_cfg_t g_external_irq0_cfg =
{ .channel = 0,
  .trigger = EXTERNAL_IRQ_TRIG_RISING,
  .filter_enable = false,
  .pclk_div = EXTERNAL_IRQ_PCLK_DIV_BY_64,
  .p_callback = NULL,
  .p_context = NULL,
  .p_extend = NULL,
  .ipl = (12),
#if defined(VECTOR_NUMBER_ICU_IRQ0)
    .irq                 = VECTOR_NUMBER_ICU_IRQ0,
#else
  .irq = FSP_INVALID_VECTOR,
#endif
        };
/* Instance structure to use this module. */
const external_irq_instance_t g_external_irq0 =
{ .p_ctrl = &g_external_irq0_ctrl, .p_cfg = &g_external_irq0_cfg, .p_api = &g_external_irq_on_icu };
gpt_instance_ctrl_t g_timer_clk_ctrl;
#if 0
const gpt_extended_pwm_cfg_t g_timer_clk_pwm_extend =
{
    .trough_ipl          = (BSP_IRQ_DISABLED),
#if defined(VECTOR_NUMBER_GPT0_COUNTER_UNDERFLOW)
    .trough_irq          = VECTOR_NUMBER_GPT0_COUNTER_UNDERFLOW,
#else
    .trough_irq          = FSP_INVALID_VECTOR,
#endif
    .poeg_link           = GPT_POEG_LINK_POEG0,
    .output_disable      =  GPT_OUTPUT_DISABLE_NONE,
    .adc_trigger         =  GPT_ADC_TRIGGER_NONE,
    .dead_time_count_up  = 0,
    .dead_time_count_down = 0,
    .adc_a_compare_match = 0,
    .adc_b_compare_match = 0,
    .interrupt_skip_source = GPT_INTERRUPT_SKIP_SOURCE_NONE,
    .interrupt_skip_count  = GPT_INTERRUPT_SKIP_COUNT_0,
    .interrupt_skip_adc    = GPT_INTERRUPT_SKIP_ADC_NONE,
    .gtioca_disable_setting = GPT_GTIOC_DISABLE_PROHIBITED,
    .gtiocb_disable_setting = GPT_GTIOC_DISABLE_PROHIBITED,
};
#endif
const gpt_extended_cfg_t g_timer_clk_extend =
        { .gtioca =
        { .output_enabled = true, .stop_level = GPT_PIN_LEVEL_HIGH },
          .gtiocb =
          { .output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW },
          .shortest_pwm_signal = GPT_SHORTEST_LEVEL_ON, .start_source = GPT_SOURCE_GPT_A | GPT_SOURCE_NONE, .stop_source =
                  GPT_SOURCE_GPT_B | GPT_SOURCE_NONE,
          .clear_source = GPT_SOURCE_GPT_A | GPT_SOURCE_NONE, .count_up_source = GPT_SOURCE_NONE, .count_down_source =
                  GPT_SOURCE_NONE,
          .capture_a_source = GPT_SOURCE_NONE, .capture_b_source = GPT_SOURCE_NONE, .capture_a_ipl = 2, .capture_b_ipl =
                  (BSP_IRQ_DISABLED),
#if defined(VECTOR_NUMBER_GPT0_CAPTURE_COMPARE_A)
    .capture_a_irq       = VECTOR_NUMBER_GPT0_CAPTURE_COMPARE_A,
#else
          .capture_a_irq = FSP_INVALID_VECTOR,
#endif
#if defined(VECTOR_NUMBER_GPT0_CAPTURE_COMPARE_B)
    .capture_b_irq       = VECTOR_NUMBER_GPT0_CAPTURE_COMPARE_B,
#else
          .capture_b_irq = FSP_INVALID_VECTOR,
#endif
          .capture_filter_gtioca = GPT_CAPTURE_FILTER_NONE,
          .capture_filter_gtiocb = GPT_CAPTURE_FILTER_NONE,
#if 0
    .p_pwm_cfg                   = &g_timer_clk_pwm_extend,
#else
          .p_pwm_cfg = NULL,
#endif
        };
const timer_cfg_t g_timer_clk_cfg =
{ .mode = TIMER_MODE_PWM,
  /* Actual period: 2.5e-7 seconds. Actual duty: 50%. */.period_counts = (uint32_t) 0x1e,
  .duty_cycle_counts = 0xf,
  .source_div = (timer_source_div_t) 0,
  .channel = 0,
  .p_callback = led_clk_callback,
  .p_context = NULL,
  .p_extend = &g_timer_clk_extend,
  .cycle_end_ipl = (2),
#if defined(VECTOR_NUMBER_GPT0_COUNTER_OVERFLOW)
    .cycle_end_irq       = VECTOR_NUMBER_GPT0_COUNTER_OVERFLOW,
#else
  .cycle_end_irq = FSP_INVALID_VECTOR,
#endif
        };
/* Instance structure to use this module. */
const timer_instance_t g_timer_clk =
{ .p_ctrl = &g_timer_clk_ctrl, .p_cfg = &g_timer_clk_cfg, .p_api = &g_timer_on_gpt };
void g_hal_init(void)
{
    g_common_init ();
}
