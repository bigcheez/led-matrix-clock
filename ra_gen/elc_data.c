/* generated ELC source file - do not edit */
#include "r_elc_api.h"
const elc_cfg_t g_elc_cfg =
{ .link[ELC_PERIPHERAL_GPT_A] = ELC_EVENT_GPT1_COUNTER_OVERFLOW, /* GPT1 COUNTER OVERFLOW (Overflow) */
  .link[ELC_PERIPHERAL_GPT_B] = ELC_EVENT_GPT1_CAPTURE_COMPARE_A, /* GPT1 CAPTURE COMPARE A (Compare match A) */
  .link[ELC_PERIPHERAL_GPT_C] = ELC_EVENT_NONE, /* No allocation */
  .link[ELC_PERIPHERAL_GPT_D] = ELC_EVENT_NONE, /* No allocation */
  .link[ELC_PERIPHERAL_GPT_E] = ELC_EVENT_NONE, /* No allocation */
  .link[ELC_PERIPHERAL_GPT_F] = ELC_EVENT_NONE, /* No allocation */
  .link[ELC_PERIPHERAL_GPT_G] = ELC_EVENT_NONE, /* No allocation */
  .link[ELC_PERIPHERAL_GPT_H] = ELC_EVENT_NONE, /* No allocation */
  .link[ELC_PERIPHERAL_ADC0] = ELC_EVENT_NONE, /* No allocation */
  .link[ELC_PERIPHERAL_ADC0_B] = ELC_EVENT_NONE, /* No allocation */
  .link[ELC_PERIPHERAL_ADC1] = ELC_EVENT_NONE, /* No allocation */
  .link[ELC_PERIPHERAL_ADC1_B] = ELC_EVENT_NONE, /* No allocation */
  .link[ELC_PERIPHERAL_DAC0] = ELC_EVENT_NONE, /* No allocation */
  .link[ELC_PERIPHERAL_DAC1] = ELC_EVENT_NONE, /* No allocation */
  .link[ELC_PERIPHERAL_IOPORT1] = ELC_EVENT_GPT0_CAPTURE_COMPARE_A, /* GPT0 CAPTURE COMPARE A (Compare match A) */
  .link[ELC_PERIPHERAL_IOPORT2] = ELC_EVENT_NONE, /* No allocation */
  .link[ELC_PERIPHERAL_IOPORT3] = ELC_EVENT_NONE, /* No allocation */
  .link[ELC_PERIPHERAL_IOPORT4] = ELC_EVENT_NONE, /* No allocation */
  .link[ELC_PERIPHERAL_CTSU] = ELC_EVENT_NONE, /* No allocation */
};
