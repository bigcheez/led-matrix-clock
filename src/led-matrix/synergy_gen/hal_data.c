/* generated HAL source file - do not edit */
#include "hal_data.h"
#if EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_green_2
#if defined(__ICCARM__)
#define NULL_WEAK_ATTRIBUTE
#pragma weak NULL                            = NULL_internal
#elif defined(__GNUC__)
#define NULL_WEAK_ATTRIBUTE       __attribute__ ((weak, alias("NULL_internal")))
#endif
void NULL(external_irq_callback_args_t * p_args) NULL_WEAK_ATTRIBUTE;
#endif
static external_irq_ctrl_t g_switch_green_2_ctrl;
static const external_irq_cfg_t g_switch_green_2_cfg =
{ .channel = 15, .trigger = EXTERNAL_IRQ_TRIG_FALLING, .filter_enable = false, .pclk_div = EXTERNAL_IRQ_PCLK_DIV_BY_64,
  .autostart = true, .p_callback = NULL, .p_context = &g_switch_green_2, .p_extend = NULL };
/* Instance structure to use this module. */
const external_irq_instance_t g_switch_green_2 =
{ .p_ctrl = &g_switch_green_2_ctrl, .p_cfg = &g_switch_green_2_cfg, .p_api = &g_external_irq_on_icu };

#if EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_green_2
/*******************************************************************************************************************//**
 * @brief      This is a weak example callback function.  It should be overridden by defining a user callback function 
 *             with the prototype below.
 *               - void NULL(external_irq_callback_args_t * p_args)
 *
 * @param[in]  p_args  Callback arguments used to identify what caused the callback.
 **********************************************************************************************************************/
void NULL_internal(external_irq_callback_args_t * p_args)
{
    /** Do nothing. */
}
#endif
#if EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_green_1
#if defined(__ICCARM__)
#define NULL_WEAK_ATTRIBUTE
#pragma weak NULL                            = NULL_internal
#elif defined(__GNUC__)
#define NULL_WEAK_ATTRIBUTE       __attribute__ ((weak, alias("NULL_internal")))
#endif
void NULL(external_irq_callback_args_t * p_args) NULL_WEAK_ATTRIBUTE;
#endif
static external_irq_ctrl_t g_switch_green_1_ctrl;
static const external_irq_cfg_t g_switch_green_1_cfg =
{ .channel = 14, .trigger = EXTERNAL_IRQ_TRIG_FALLING, .filter_enable = false, .pclk_div = EXTERNAL_IRQ_PCLK_DIV_BY_64,
  .autostart = true, .p_callback = NULL, .p_context = &g_switch_green_1, .p_extend = NULL };
/* Instance structure to use this module. */
const external_irq_instance_t g_switch_green_1 =
{ .p_ctrl = &g_switch_green_1_ctrl, .p_cfg = &g_switch_green_1_cfg, .p_api = &g_external_irq_on_icu };

#if EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_green_1
/*******************************************************************************************************************//**
 * @brief      This is a weak example callback function.  It should be overridden by defining a user callback function 
 *             with the prototype below.
 *               - void NULL(external_irq_callback_args_t * p_args)
 *
 * @param[in]  p_args  Callback arguments used to identify what caused the callback.
 **********************************************************************************************************************/
void NULL_internal(external_irq_callback_args_t * p_args)
{
    /** Do nothing. */
}
#endif
#if EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_red_2
#if defined(__ICCARM__)
#define NULL_WEAK_ATTRIBUTE
#pragma weak NULL                            = NULL_internal
#elif defined(__GNUC__)
#define NULL_WEAK_ATTRIBUTE       __attribute__ ((weak, alias("NULL_internal")))
#endif
void NULL(external_irq_callback_args_t * p_args) NULL_WEAK_ATTRIBUTE;
#endif
static external_irq_ctrl_t g_switch_red_2_ctrl;
static const external_irq_cfg_t g_switch_red_2_cfg =
{ .channel = 11, .trigger = EXTERNAL_IRQ_TRIG_FALLING, .filter_enable = false, .pclk_div = EXTERNAL_IRQ_PCLK_DIV_BY_64,
  .autostart = true, .p_callback = NULL, .p_context = &g_switch_red_2, .p_extend = NULL };
/* Instance structure to use this module. */
const external_irq_instance_t g_switch_red_2 =
{ .p_ctrl = &g_switch_red_2_ctrl, .p_cfg = &g_switch_red_2_cfg, .p_api = &g_external_irq_on_icu };

#if EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_red_2
/*******************************************************************************************************************//**
 * @brief      This is a weak example callback function.  It should be overridden by defining a user callback function 
 *             with the prototype below.
 *               - void NULL(external_irq_callback_args_t * p_args)
 *
 * @param[in]  p_args  Callback arguments used to identify what caused the callback.
 **********************************************************************************************************************/
void NULL_internal(external_irq_callback_args_t * p_args)
{
    /** Do nothing. */
}
#endif
#if EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_red_1
#if defined(__ICCARM__)
#define switch_callback_WEAK_ATTRIBUTE
#pragma weak switch_callback                            = switch_callback_internal
#elif defined(__GNUC__)
#define switch_callback_WEAK_ATTRIBUTE       __attribute__ ((weak, alias("switch_callback_internal")))
#endif
void switch_callback(external_irq_callback_args_t * p_args) switch_callback_WEAK_ATTRIBUTE;
#endif
static external_irq_ctrl_t g_switch_red_1_ctrl;
static const external_irq_cfg_t g_switch_red_1_cfg =
{ .channel = 9, .trigger = EXTERNAL_IRQ_TRIG_FALLING, .filter_enable = false, .pclk_div = EXTERNAL_IRQ_PCLK_DIV_BY_64,
  .autostart = true, .p_callback = switch_callback, .p_context = &g_switch_red_1, .p_extend = NULL };
/* Instance structure to use this module. */
const external_irq_instance_t g_switch_red_1 =
{ .p_ctrl = &g_switch_red_1_ctrl, .p_cfg = &g_switch_red_1_cfg, .p_api = &g_external_irq_on_icu };

#if EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_red_1
/*******************************************************************************************************************//**
 * @brief      This is a weak example callback function.  It should be overridden by defining a user callback function 
 *             with the prototype below.
 *               - void switch_callback(external_irq_callback_args_t * p_args)
 *
 * @param[in]  p_args  Callback arguments used to identify what caused the callback.
 **********************************************************************************************************************/
void switch_callback_internal(external_irq_callback_args_t * p_args)
{
    /** Do nothing. */
}
#endif
#if TIMER_ON_GPT_CALLBACK_USED_g_timer_graphic
#if defined(__ICCARM__)
#define timer_change_graphic_callback_WEAK_ATTRIBUTE
#pragma weak timer_change_graphic_callback                            = timer_change_graphic_callback_internal
#elif defined(__GNUC__)
#define timer_change_graphic_callback_WEAK_ATTRIBUTE       __attribute__ ((weak, alias("timer_change_graphic_callback_internal")))
#endif
void timer_change_graphic_callback(timer_callback_args_t * p_args) timer_change_graphic_callback_WEAK_ATTRIBUTE;
#endif
static timer_ctrl_t g_timer_graphic_ctrl;
static const timer_on_gpt_cfg_t g_timer_graphic_extend =
{ .gtioca.output_enabled = false, .gtioca.stop_level = GPT_PIN_LEVEL_LOW, .gtiocb.output_enabled = false,
  .gtiocb.stop_level = GPT_PIN_LEVEL_LOW, };
static const timer_cfg_t g_timer_graphic_cfg =
{ .mode = TIMER_MODE_PERIODIC, .period = 100, .unit = TIMER_UNIT_PERIOD_MSEC, .duty_cycle = 50, .duty_cycle_unit =
          TIMER_PWM_UNIT_RAW_COUNTS,
  .channel = 2, .autostart = true, .p_callback = timer_change_graphic_callback, .p_context = &g_timer_graphic,
  .p_extend = &g_timer_graphic_extend };
/* Instance structure to use this module. */
const timer_instance_t g_timer_graphic =
{ .p_ctrl = &g_timer_graphic_ctrl, .p_cfg = &g_timer_graphic_cfg, .p_api = &g_timer_on_gpt };

#if TIMER_ON_GPT_CALLBACK_USED_g_timer_graphic
/*******************************************************************************************************************//**
 * @brief      This is a weak example callback function.  It should be overridden by defining a user callback function 
 *             with the prototype below.
 *               - void timer_change_graphic_callback(timer_callback_args_t * p_args)
 *
 * @param[in]  p_args  Callback arguments used to identify what caused the callback.
 **********************************************************************************************************************/
void timer_change_graphic_callback_internal(timer_callback_args_t * p_args)
{
    /** Do nothing. */
}
#endif
#if TIMER_ON_GPT_CALLBACK_USED_g_timer_bit
#if defined(__ICCARM__)
#define NULL_WEAK_ATTRIBUTE
#pragma weak NULL                            = NULL_internal
#elif defined(__GNUC__)
#define NULL_WEAK_ATTRIBUTE       __attribute__ ((weak, alias("NULL_internal")))
#endif
void NULL(timer_callback_args_t * p_args) NULL_WEAK_ATTRIBUTE;
#endif
static timer_ctrl_t g_timer_bit_ctrl;
static const timer_on_gpt_cfg_t g_timer_bit_extend =
{ .gtioca.output_enabled = false, .gtioca.stop_level = GPT_PIN_LEVEL_LOW, .gtiocb.output_enabled = true,
  .gtiocb.stop_level = GPT_PIN_LEVEL_LOW, };
static const timer_cfg_t g_timer_bit_cfg =
{ .mode = TIMER_MODE_PWM, .period = 17000, .unit = TIMER_UNIT_PERIOD_NSEC, .duty_cycle = 50, .duty_cycle_unit =
          TIMER_PWM_UNIT_PERCENT,
  .channel = 1, .autostart = false, .p_callback = NULL, .p_context = &g_timer_bit, .p_extend = &g_timer_bit_extend };
/* Instance structure to use this module. */
const timer_instance_t g_timer_bit =
{ .p_ctrl = &g_timer_bit_ctrl, .p_cfg = &g_timer_bit_cfg, .p_api = &g_timer_on_gpt };

#if TIMER_ON_GPT_CALLBACK_USED_g_timer_bit
/*******************************************************************************************************************//**
 * @brief      This is a weak example callback function.  It should be overridden by defining a user callback function 
 *             with the prototype below.
 *               - void NULL(timer_callback_args_t * p_args)
 *
 * @param[in]  p_args  Callback arguments used to identify what caused the callback.
 **********************************************************************************************************************/
void NULL_internal(timer_callback_args_t * p_args)
{
    /** Do nothing. */
}
#endif
#ifdef NULL
#define DTC_CALLBACK_USED_g_transfer_gpt_period (0)
#else
#define DTC_CALLBACK_USED_g_transfer_gpt_period (1)
#endif
#if DTC_CALLBACK_USED_g_transfer_gpt_period
void NULL(transfer_callback_args_t * p_args);
#endif
transfer_ctrl_t g_transfer_gpt_period_ctrl;
transfer_info_t g_transfer_gpt_period_info =
{ .dest_addr_mode = TRANSFER_ADDR_MODE_FIXED, .repeat_area = TRANSFER_REPEAT_AREA_SOURCE, .irq = TRANSFER_IRQ_END,
  .chain_mode = TRANSFER_CHAIN_MODE_DISABLED, .src_addr_mode = TRANSFER_ADDR_MODE_INCREMENTED, .size =
          TRANSFER_SIZE_4_BYTE,
  .mode = TRANSFER_MODE_REPEAT, .p_dest = (void * volatile ) &R_GPTA1->GTPR, .p_src = (void const * volatile ) NULL,
  .num_blocks = 0, .length = 6, };
const transfer_cfg_t g_transfer_gpt_period_cfg =
{ .p_info = &g_transfer_gpt_period_info, .activation_source = ELC_EVENT_GPT1_COUNTER_OVERFLOW, .auto_enable = true,
  .p_callback = NULL, .p_context = &g_transfer_gpt_period, };
/* Instance structure to use this module. */
const transfer_instance_t g_transfer_gpt_period =
{ .p_ctrl = &g_transfer_gpt_period_ctrl, .p_cfg = &g_transfer_gpt_period_cfg, .p_api = &g_transfer_on_dtc };
#ifdef NULL
#define DTC_CALLBACK_USED_g_transfer (0)
#else
#define DTC_CALLBACK_USED_g_transfer (1)
#endif
#if DTC_CALLBACK_USED_g_transfer
void NULL(transfer_callback_args_t * p_args);
#endif
transfer_ctrl_t g_transfer_ctrl;
transfer_info_t g_transfer_info =
{ .dest_addr_mode = TRANSFER_ADDR_MODE_FIXED, .repeat_area = TRANSFER_REPEAT_AREA_SOURCE, .irq = TRANSFER_IRQ_END,
  .chain_mode = TRANSFER_CHAIN_MODE_DISABLED, .src_addr_mode = TRANSFER_ADDR_MODE_INCREMENTED, .size =
          TRANSFER_SIZE_4_BYTE,
  .mode = TRANSFER_MODE_NORMAL, .p_dest = (void * volatile ) &R_IOPORT3->PCNTR4, .p_src = (void const * volatile ) NULL,
  .num_blocks = 0, .length = 6240, };
const transfer_cfg_t g_transfer_cfg =
{ .p_info = &g_transfer_info, .activation_source = ELC_EVENT_GPT0_CAPTURE_COMPARE_A, .auto_enable = false, .p_callback =
          NULL,
  .p_context = &g_transfer, };
/* Instance structure to use this module. */
const transfer_instance_t g_transfer =
{ .p_ctrl = &g_transfer_ctrl, .p_cfg = &g_transfer_cfg, .p_api = &g_transfer_on_dtc };
#if TIMER_ON_GPT_CALLBACK_USED_g_timer_clk
#if defined(__ICCARM__)
#define NULL_WEAK_ATTRIBUTE
#pragma weak NULL                            = NULL_internal
#elif defined(__GNUC__)
#define NULL_WEAK_ATTRIBUTE       __attribute__ ((weak, alias("NULL_internal")))
#endif
void NULL(timer_callback_args_t * p_args) NULL_WEAK_ATTRIBUTE;
#endif
static timer_ctrl_t g_timer_clk_ctrl;
static const timer_on_gpt_cfg_t g_timer_clk_extend =
{ .gtioca.output_enabled = true, .gtioca.stop_level = GPT_PIN_LEVEL_HIGH, .gtiocb.output_enabled = false,
  .gtiocb.stop_level = GPT_PIN_LEVEL_LOW, };
static const timer_cfg_t g_timer_clk_cfg =
{ .mode = TIMER_MODE_PWM, .period = 250, .unit = TIMER_UNIT_PERIOD_NSEC, .duty_cycle = 50, .duty_cycle_unit =
          TIMER_PWM_UNIT_PERCENT,
  .channel = 0, .autostart = false, .p_callback = NULL, .p_context = &g_timer_clk, .p_extend = &g_timer_clk_extend };
/* Instance structure to use this module. */
const timer_instance_t g_timer_clk =
{ .p_ctrl = &g_timer_clk_ctrl, .p_cfg = &g_timer_clk_cfg, .p_api = &g_timer_on_gpt };

#if TIMER_ON_GPT_CALLBACK_USED_g_timer_clk
/*******************************************************************************************************************//**
 * @brief      This is a weak example callback function.  It should be overridden by defining a user callback function 
 *             with the prototype below.
 *               - void NULL(timer_callback_args_t * p_args)
 *
 * @param[in]  p_args  Callback arguments used to identify what caused the callback.
 **********************************************************************************************************************/
void NULL_internal(timer_callback_args_t * p_args)
{
    /** Do nothing. */
}
#endif
const ioport_instance_t g_ioport =
{ .p_api = &g_ioport_on_ioport, .p_cfg = NULL };
const elc_instance_t g_elc =
{ .p_api = &g_elc_on_elc, .p_cfg = NULL };
const cgc_instance_t g_cgc =
{ .p_api = &g_cgc_on_cgc, .p_cfg = NULL };
void g_hal_init(void)
{
}
