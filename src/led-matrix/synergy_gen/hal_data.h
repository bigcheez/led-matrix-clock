/* generated HAL header file - do not edit */
#ifndef HAL_DATA_H_
#define HAL_DATA_H_
#include <stdint.h>
#include "bsp_api.h"
#include "r_icu.h"
#include "r_external_irq_api.h"
#include "r_gpt.h"
#include "r_timer_api.h"
#include "r_dtc.h"
#include "r_transfer_api.h"
#include "r_ioport.h"
#include "r_ioport_api.h"
#include "r_elc.h"
#include "r_elc_api.h"
#include "r_cgc.h"
#include "r_cgc_api.h"
/* External IRQ on ICU Instance. */
extern const external_irq_instance_t g_switch_green_2;
#ifdef NULL
#define EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_green_2 (0)
#else
#define EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_green_2 (1)
#endif
#if EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_green_2
void NULL(external_irq_callback_args_t * p_args);
#endif
/* External IRQ on ICU Instance. */
extern const external_irq_instance_t g_switch_green_1;
#ifdef NULL
#define EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_green_1 (0)
#else
#define EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_green_1 (1)
#endif
#if EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_green_1
void NULL(external_irq_callback_args_t * p_args);
#endif
/* External IRQ on ICU Instance. */
extern const external_irq_instance_t g_switch_red_2;
#ifdef NULL
#define EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_red_2 (0)
#else
#define EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_red_2 (1)
#endif
#if EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_red_2
void NULL(external_irq_callback_args_t * p_args);
#endif
/* External IRQ on ICU Instance. */
extern const external_irq_instance_t g_switch_red_1;
#ifdef switch_callback
#define EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_red_1 (0)
#else
#define EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_red_1 (1)
#endif
#if EXTERNAL_IRQ_ON_ICU_CALLBACK_USED_g_switch_red_1
void switch_callback(external_irq_callback_args_t * p_args);
#endif
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer_graphic;
#ifdef timer_change_graphic_callback
#define TIMER_ON_GPT_CALLBACK_USED_g_timer_graphic (0)
#else
#define TIMER_ON_GPT_CALLBACK_USED_g_timer_graphic (1)
#endif
#if TIMER_ON_GPT_CALLBACK_USED_g_timer_graphic
void timer_change_graphic_callback(timer_callback_args_t * p_args);
#endif
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer_bit;
#ifdef NULL
#define TIMER_ON_GPT_CALLBACK_USED_g_timer_bit (0)
#else
#define TIMER_ON_GPT_CALLBACK_USED_g_timer_bit (1)
#endif
#if TIMER_ON_GPT_CALLBACK_USED_g_timer_bit
void NULL(timer_callback_args_t * p_args);
#endif
/* Transfer on DTC Instance. */
extern const transfer_instance_t g_transfer_gpt_period;
/* Transfer on DTC Instance. */
extern const transfer_instance_t g_transfer;
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer_clk;
#ifdef NULL
#define TIMER_ON_GPT_CALLBACK_USED_g_timer_clk (0)
#else
#define TIMER_ON_GPT_CALLBACK_USED_g_timer_clk (1)
#endif
#if TIMER_ON_GPT_CALLBACK_USED_g_timer_clk
void NULL(timer_callback_args_t * p_args);
#endif
/** IOPORT Instance */
extern const ioport_instance_t g_ioport;
/** ELC Instance */
extern const elc_instance_t g_elc;
/** CGC Instance */
extern const cgc_instance_t g_cgc;
/** Prototypes for generated HAL functions. */
void hal_entry(void);
void g_hal_init(void);
#endif /* HAL_DATA_H_ */
