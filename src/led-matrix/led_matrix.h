/*
 * led_matrix.h
 *
 *  Created on: Oct 10, 2015
 *      Author: Bhussey01
 */

#ifndef LED_MATRIX_H_
#define LED_MATRIX_H_

#include "r_ioport.h"

#define LED_MATRIX_ROWS                 (32)
#define LED_MATRIX_COLS                 (32)
#define LED_MATRIX_DUMMY_COLS           (1)     ///< +1 for using dummy column at beginning of line to prime DTC
#define LED_MATRIX_PANELS               (1)
#define LED_MATRIX_PIXELS               (LED_MATRIX_ROWS * LED_MATRIX_COLS)
#define LED_MATRIX_ROWS_AT_ONCE         (2)
#define LED_MATRIX_ROWS_AT_ONCE_DELTA   (16)
#define LED_MATRIX_COLOR_DEPTH          (6)
#define LED_MATRIX_DATA_MASK            (0x00007FF8uL)

extern const uint8_t g_led_matrix_gamma[256];
extern const uint8_t g_led_matrix_letters[95][13];

typedef struct st_led_matrix_pins
{
    ioport_port_pin_t r1;
    ioport_port_pin_t g1;
    ioport_port_pin_t b1;
    ioport_port_pin_t r2;
    ioport_port_pin_t g2;
    ioport_port_pin_t b2;
    ioport_port_pin_t a;
    ioport_port_pin_t b;
    ioport_port_pin_t c;
    ioport_port_pin_t d;
    ioport_port_pin_t clk;
    ioport_port_pin_t lat;
    ioport_port_pin_t oe;
} led_matrix_pins;

typedef struct st_led_matrix_pixel
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
} led_matrix_pixel_t;

typedef struct st_led_matrix_frame
{
    uint32_t color_depth;
    uint32_t width;
    uint32_t height;
    uint32_t panel_number;
    led_matrix_pixel_t * p_data;
} led_matrix_frame_t;

typedef struct st_led_matrix_panel_frame_half
{
    union
    {
        struct {
            uint8_t         : 3;
            uint8_t r1      : 1;
            uint8_t g1      : 1;
            uint8_t b1      : 1;
            uint8_t r2      : 1;
            uint8_t g2      : 1;
            uint8_t b2      : 1;
            uint8_t addr    : 4;
            uint8_t lat     : 1;
            uint8_t oe      : 1;
            uint8_t         : 1;
        } bits;

        uint16_t word;
    };
} led_matrix_panel_frame_half_t;

typedef struct st_led_matrix_panel_frame
{
    led_matrix_panel_frame_half_t set;      ///< Bottom half is for setting bits
    led_matrix_panel_frame_half_t clear;    ///< Top half is for clearing bits
} led_matrix_panel_frame_t;

typedef struct st_led_matrix_images
{
    uint32_t        frames;
    uint32_t        loops;
    uint32_t const  (* p_data)[LED_MATRIX_PIXELS];
} led_matrix_images_t;

typedef struct st_led_matrix_sprite
{
    uint32_t   width;
    uint32_t   height;
    uint32_t   x;
    uint32_t   y;
    uint32_t * p_data;
} led_matrix_sprite_t;

typedef struct st_led_matrix_panel_info
{
    led_matrix_images_t *   images;
    led_matrix_pixel_t *    transparencies;
    uint32_t                transparencies_count;
    uint32_t                current_image;
    uint32_t                current_loop;
    uint32_t                current_frame;
    uint32_t                current_transparency;
    led_matrix_pixel_t      transparency;
    uint32_t                updates_per_image;
} led_matrix_panel_info_t;

void led_matrix_parse(led_matrix_frame_t * p_frame, led_matrix_panel_frame_t * p_output, led_matrix_pixel_t const * p_transparency);

#endif /* LED_MATRIX_H_ */
