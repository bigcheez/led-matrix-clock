/*
 * led_matrix_channels.h
 *
 *  Created on: Oct 28, 2015
 *      Author: Bhussey01
 */

#ifndef LED_MATRIX_CHANNELS_H_
#define LED_MATRIX_CHANNELS_H_

#include "led_matrix.h"

typedef enum e_led_matrix_channels
{
    LED_MATRIX_CHANNEL_ALL = 0,
    LED_MATRIX_CHANNEL_NCSU,
    LED_MATRIX_CHANNEL_DISNEY,
    LED_MATRIX_CHANNEL_HALLOWEEN,
} led_matrix_channel_t;

#endif /* LED_MATRIX_CHANNELS_H_ */
