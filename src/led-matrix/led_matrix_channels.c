/*
 * led_matrix_channels.c
 *
 *  Created on: Oct 28, 2015
 *      Author: Bhussey01
 */

#include "led_matrix.h"
#include "led_matrix_images.h"

const led_matrix_images_t g_images_panel_0_all[] =
{
#if 1
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_baymax_head[0],
    },
#if 0
    {
         .frames = 1,
         .loops  = 1,
         .p_data = &g_led_matrix_data_font[0],
    },
#endif
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_mickey_upper[0],
    },
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_mickey_whole_1[0],
    },
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_lightning[0],
    },
    {
        .frames = 6,
        .loops  = 2,
        .p_data = &g_led_matrix_data_raw_mickey_running[0],
    },
#endif
#if 0
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_test_square[0],
    },
#endif
#if 1
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_jake[0],
    },
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_state[0],
    },
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_tuffy[0],
    },
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_baymax_1[0],
    },
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_buzz[0],
    },
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_pjmasks[0],
    },
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_pumpkin[0],
    },
    {
        .frames = 14,
        .loops  = 4,
        .p_data = &g_led_matrix_data_raw_bat[0],
    },
    {
        .frames = 4,
        .loops  = 10,
        .p_data = &g_led_matrix_data_raw_fire[0],
    },
#endif
    { ///< Empty last image to mark end
        .frames = 0,
        .loops  = 0,
        .p_data = NULL,
    },
};

const led_matrix_images_t g_images_panel_0_ncsu[] =
{
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_state[0],
    },
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_tuffy[0],
    },
    { ///< Empty last image to mark end
        .frames = 0,
        .loops  = 0,
        .p_data = NULL,
    },
};

const led_matrix_images_t g_images_panel_0_disney[] =
{
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_baymax_head[0],
    },
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_mickey_upper[0],
    },
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_mickey_whole_1[0],
    },
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_lightning[0],
    },
    {
        .frames = 6,
        .loops  = 2,
        .p_data = &g_led_matrix_data_raw_mickey_running[0],
    },
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_jake[0],
    },
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_baymax_1[0],
    },
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_buzz[0],
    },
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_pjmasks[0],
    },
    { ///< Empty last image to mark end
        .frames = 0,
        .loops  = 0,
        .p_data = NULL,
    },
};

const led_matrix_images_t g_images_panel_0_halloween[] =
{
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_pumpkin[0],
    },
    {
        .frames = 14,
        .loops  = 4,
        .p_data = &g_led_matrix_data_raw_bat[0],
    },
    {
        .frames = 4,
        .loops  = 10,
        .p_data = &g_led_matrix_data_raw_fire[0],
    },
    { ///< Empty last image to mark end
        .frames = 0,
        .loops  = 0,
        .p_data = NULL,
    },
};

const led_matrix_images_t * g_images_panel_0[] =
{
     &g_images_panel_0_all[0],
     &g_images_panel_0_ncsu[0],
     &g_images_panel_0_disney[0],
     &g_images_panel_0_halloween[0],
};

#if LED_MATRIX_PANELS > 1

const led_matrix_images_t g_images_panel_1_all[] =
{
#if 1
    {
        .frames = 4,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_baymax_eyes[0],
    },
    {
        .frames = 1,
        .loops  = 10,
        .p_data = &g_led_matrix_data_raw_baymax_eyes[0],
    },
    {
        .frames = 4,
        .loops  = 2,
        .p_data = &g_led_matrix_data_raw_baymax_eyes[0],
    },
    {
        .frames = 1,
        .loops  = 6,
        .p_data = &g_led_matrix_data_raw_eyes[0],
    },
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_eyes[1],
    },
    {
        .frames = 1,
        .loops  = 6,
        .p_data = &g_led_matrix_data_raw_eyes[2],
    },
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_eyes[3],
    },
    {
        .frames = 1,
        .loops  = 6,
        .p_data = &g_led_matrix_data_raw_eyes[4],
    },
    {
        .frames = 1,
        .loops  = 1,
        .p_data = &g_led_matrix_data_raw_eyes[5],
    },
    {
        .frames = 1,
        .loops  = 6,
        .p_data = &g_led_matrix_data_raw_eyes[0],
    },
    {
        .frames = 19,
        .loops  = 16,
        .p_data = &g_led_matrix_data_raw_cylon[0],
    },
#endif
    { ///< Empty last image to mark end
        .frames = 0,
        .loops  = 0,
        .p_data = NULL,
    },
};

const led_matrix_images_t * g_images_panel_1[] =
{
     &g_images_panel_1_all[0],
     &g_images_panel_1_all[0],
     &g_images_panel_1_all[0],
     &g_images_panel_1_all[0],
};

#endif
