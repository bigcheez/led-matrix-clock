/*
 * led_matrix_images.h
 *
 *  Created on: Oct 28, 2015
 *      Author: Bhussey01
 */

#ifndef LED_MATRIX_IMAGES_H_
#define LED_MATRIX_IMAGES_H_

#include "led_matrix.h"

extern const uint32_t g_led_matrix_data_stripes[];
extern const uint32_t g_led_matrix_data_stripes_holes[];
extern const uint32_t g_led_matrix_data_mickey[];
extern const uint32_t g_led_matrix_data_baymax_suit[];
extern const uint32_t g_led_matrix_data_baymax_eyes_red[];
extern const uint32_t g_led_matrix_data_baymax_eyes_blue[];
extern const uint32_t g_led_matrix_data_baymax_eyes_green[];
extern const uint32_t g_led_matrix_data_baymax_eyes_orange[];
extern const uint32_t g_led_matrix_data_gradient_red[];
extern const uint32_t g_led_matrix_data_test[];

extern const uint32_t g_led_matrix_data_raw_mickey_rect[];
extern const uint32_t g_led_matrix_data_raw_mickey_square[];
extern const uint32_t g_led_matrix_data_raw_test[];

extern const uint32_t g_led_matrix_data_raw_test_square[1][1024];
extern const uint32_t g_led_matrix_data_raw_baymax_head[1][1024];
extern const uint32_t g_led_matrix_data_raw_baymax_1[1][1024];
extern const uint32_t g_led_matrix_data_raw_mickey_running[6][1024];
extern const uint32_t g_led_matrix_data_raw_bat[14][1024];
extern const uint32_t g_led_matrix_data_raw_fire[4][1024];
extern const uint32_t g_led_matrix_data_raw_mickey_upper[1][1024];
extern const uint32_t g_led_matrix_data_raw_mickey_whole_1[1][1024];
extern const uint32_t g_led_matrix_data_raw_lightning[1][1024];
extern const uint32_t g_led_matrix_data_raw_jake[1][1024];
extern const uint32_t g_led_matrix_data_raw_state[1][1024];
extern const uint32_t g_led_matrix_data_raw_tuffy[1][1024];
extern const uint32_t g_led_matrix_data_raw_buzz[1][1024];
extern const uint32_t g_led_matrix_data_raw_pumpkin[1][1024];
extern const uint32_t g_led_matrix_data_raw_pjmasks[1][1024];

/** 32x16 panel */
extern const uint32_t g_led_matrix_data_raw_baymax_eyes[4][1024];
extern const uint32_t g_led_matrix_data_raw_eyes[6][1024];
extern const uint32_t g_led_matrix_data_raw_cylon[19][1024];

#endif /* LED_MATRIX_IMAGES_H_ */
