/*
 * led_matrix_parse.c
 *
 *  Created on: Oct 19, 2015
 *      Author: Bhussey01
 */

#include "led_matrix.h"

void led_matrix_parse (led_matrix_frame_t * p_frame, led_matrix_panel_frame_t * p_output, led_matrix_pixel_t const * p_transparency)
{
    uint32_t depth_shift = 0;
    uint32_t max = (uint32_t)((1 << p_frame->color_depth)-1);
    led_matrix_panel_frame_t * p_tmp = p_output;

    /** Figure out how much to shift data */
    if ((p_frame->color_depth - LED_MATRIX_COLOR_DEPTH) > 0)
    {
        depth_shift = p_frame->color_depth - LED_MATRIX_COLOR_DEPTH;
    }

    for (uint32_t row = 0; row < (p_frame->height/2); row++)
    {
        for (uint32_t bit = 0; bit < LED_MATRIX_COLOR_DEPTH; bit++)
        {
            for (uint32_t col = 0; col < p_frame->width; col++)
            {
                led_matrix_pixel_t pixel_1;
                led_matrix_pixel_t pixel_2;
                float              alpha_percent;
                float              transparency_percent;

                /** Get base to this row. */
                p_tmp = &p_output[(row * LED_MATRIX_COLS * LED_MATRIX_COLOR_DEPTH * LED_MATRIX_PANELS) + (row * LED_MATRIX_DUMMY_COLS * LED_MATRIX_COLOR_DEPTH)];

                /** Adjust for column and panel number */
                p_tmp += ((bit * LED_MATRIX_COLS * LED_MATRIX_PANELS) + (bit * LED_MATRIX_DUMMY_COLS)) + col;

                /** Add on if not last panel. */
                if (p_frame->panel_number < (LED_MATRIX_PANELS - 1))
                {
                    p_tmp += (LED_MATRIX_COLS * (LED_MATRIX_PANELS - 1 - p_frame->panel_number));
                }

                pixel_1 = p_frame->p_data[(row * p_frame->width) + col];
                pixel_2 = p_frame->p_data[((row + LED_MATRIX_ROWS_AT_ONCE_DELTA) * p_frame->width) + col];

                alpha_percent = (float)pixel_1.a / (float)max;
                transparency_percent = (float)1.0 - alpha_percent;

                pixel_1.r = (uint8_t)(((float)pixel_1.r * alpha_percent) + ((float)p_transparency->r * transparency_percent));
                pixel_1.g = (uint8_t)(((float)pixel_1.g * alpha_percent) + ((float)p_transparency->g * transparency_percent));
                pixel_1.b = (uint8_t)(((float)pixel_1.b * alpha_percent) + ((float)p_transparency->b * transparency_percent));

                alpha_percent = (float)pixel_2.a / (float)max;
                transparency_percent = (float)1.0 - alpha_percent;

                pixel_2.r = (uint8_t)(((float)pixel_2.r * alpha_percent) + ((float)p_transparency->r * transparency_percent));
                pixel_2.g = (uint8_t)(((float)pixel_2.g * alpha_percent) + ((float)p_transparency->g * transparency_percent));
                pixel_2.b = (uint8_t)(((float)pixel_2.b * alpha_percent) + ((float)p_transparency->b * transparency_percent));

                /** Pixel 1 */
                p_tmp->set.bits.r1   = (uint8_t)(((g_led_matrix_gamma[pixel_1.r] >> depth_shift) >> bit) & 0x1);
                p_tmp->set.bits.g1   = (uint8_t)(((g_led_matrix_gamma[pixel_1.g] >> depth_shift) >> bit) & 0x1);
                p_tmp->set.bits.b1   = (uint8_t)(((g_led_matrix_gamma[pixel_1.b] >> depth_shift) >> bit) & 0x1);

                /** Pixel 2 */
                p_tmp->set.bits.r2   = (uint8_t)(((g_led_matrix_gamma[pixel_2.r] >> depth_shift) >> bit) & 0x1);
                p_tmp->set.bits.g2   = (uint8_t)(((g_led_matrix_gamma[pixel_2.g] >> depth_shift) >> bit) & 0x1);
                p_tmp->set.bits.b2   = (uint8_t)(((g_led_matrix_gamma[pixel_2.b] >> depth_shift) >> bit) & 0x1);

                p_tmp->set.bits.oe   = 0;       ///< Output enable
                p_tmp->set.bits.lat  = 0;       ///< Latch is low (latch after row is finished)

                p_tmp->set.bits.addr = row & (0xF);   ///< Set address bits to current line.
                p_tmp->set.bits.oe   = 0;             ///< Output enabled

                if (p_frame->panel_number == 0)
                {
                    if (col == (p_frame->width - 1))
                    {
                        p_tmp->set.bits.lat  = 1;       ///< Latch data now
                    }

                    if (bit == 0)
                    {
                        if (col <= (p_frame->width - 4))
                        {
                            /** Set address bits to last line (still showing) until we are ready to latch new data */
                            p_tmp->set.bits.addr = (row-1) & (0xF);
                        }


                        if ((col == (p_frame->width - 4)) || (col == (p_frame->width - 3)))
                        {
                            p_tmp->set.bits.oe   = 1;       ///< Disable output when changing rows
                        }
                    }
                }
                else
                {
                    if (bit == 0)
                    {
                        /** Set address bits to last line (still showing) until we are ready to latch new data */
                        p_tmp->set.bits.addr = (row-1) & (0xF);
                    }
                }

                /** Setup clear half */
                p_tmp->clear.word = (uint16_t)(((uint32_t)~p_tmp->set.word) & LED_MATRIX_DATA_MASK);

                /** Write 'dummy' data for last write. This is done for priming DTC at the beginning of the write. */
                if (p_frame->panel_number == 0)
                {
                    if (col == (p_frame->width - 1))
                    {
                        p_tmp[1].clear.word = p_tmp->clear.word;
                        p_tmp[1].set.word = p_tmp->set.word;
                    }
                }
            }
        }
    }
}
