/* HAL-only entry function */
#include "hal_data.h"
#include "led_matrix.h"
#include "led_matrix_images.h"
#include "led_matrix_channels.h"

#define GRAPHIC_PTR     g_led_matrix_data_raw_lightning
#define UPDATES_PER_IMAGE       (20)

volatile int32_t g_panel_channel_change;
extern const led_matrix_images_t * g_images_panel_0[];
extern const led_matrix_images_t * g_images_panel_1[];

uint32_t g_led_matrix_data_font[1][1024];

volatile uint32_t g_images_index;

/** Buffer for DTC data */
volatile uint32_t g_dtc_buffer[2][((LED_MATRIX_ROWS/LED_MATRIX_ROWS_AT_ONCE) * LED_MATRIX_COLS * LED_MATRIX_COLOR_DEPTH * LED_MATRIX_PANELS) + ((LED_MATRIX_ROWS/LED_MATRIX_ROWS_AT_ONCE) * LED_MATRIX_COLOR_DEPTH)];

const led_matrix_pins g_led_matrix_pins =
{
    .r1  = IOPORT_PORT_03_PIN_03,
    .g1  = IOPORT_PORT_03_PIN_04,
    .b1  = IOPORT_PORT_03_PIN_05,
    .r2  = IOPORT_PORT_03_PIN_06,
    .g2  = IOPORT_PORT_03_PIN_07,
    .b2  = IOPORT_PORT_03_PIN_08,
    .a   = IOPORT_PORT_03_PIN_09,
    .b   = IOPORT_PORT_03_PIN_10,
    .c   = IOPORT_PORT_03_PIN_11,
    .d   = IOPORT_PORT_03_PIN_12,
    .clk = IOPORT_PORT_05_PIN_12,
    .lat = IOPORT_PORT_03_PIN_13,
    .oe  = IOPORT_PORT_03_PIN_14,
};

const led_matrix_pixel_t g_transparencies[] =
{
    { ///< Bright yellow
        .r = 255,
        .g = 245,
        .b = 100,
        .a = 255
    },
    { ///< Bright blue
        .r = 0x2d,
        .g = 0xb7,
        .b = 0xcb,
        .a = 255
    },
    { ///< Purple
        .r = 0x80,
        .g = 0x30,
        .b = 0x80,
        .a = 255
    },
    { ///< Red
        .r = 0xd5,
        .g = 0x10,
        .b = 0x09,
        .a = 255
    }
};

const led_matrix_pixel_t g_pixel_white[] =
{
    {
    .r = 0xC0,
    .g = 0xC0,
    .b = 0xC0,
    .a = 0xFF,
    }
};

const led_matrix_pixel_t g_pixel_black[] =
{
    {
    .r = 0,
    .g = 0,
    .b = 0,
    .a = 0xFF,
    }
};

typedef struct st_switch_context
{
    led_matrix_channel_t channel;
} switch_context_t;

static const switch_context_t g_switch_contexts[] =
{
    { .channel = LED_MATRIX_CHANNEL_ALL },
    { .channel = LED_MATRIX_CHANNEL_DISNEY},
    { .channel = LED_MATRIX_CHANNEL_HALLOWEEN},
    { .channel = LED_MATRIX_CHANNEL_NCSU},
};

led_matrix_panel_info_t g_panel_info[LED_MATRIX_PANELS];

ioport_port_pin_t g_tmp = IOPORT_PORT_05_PIN_11;
volatile bool g_next_bit;
volatile uint32_t g_clk_count = 0;
volatile bool     g_stop_timer;
uint32_t g_bit_timer_values[LED_MATRIX_COLOR_DEPTH];
volatile bool g_change_graphic;
volatile bool g_change_dtc_src;
uint16_t  g_dtc_length;
volatile uint32_t g_counter;

bool led_matrix_pixel_merge_to(led_matrix_pixel_t * p_pixel, led_matrix_pixel_t * p_to);
void led_matrix_add_sprite(uint32_t * p_frame, led_matrix_sprite_t * p_sprite, led_matrix_pixel_t * p_pixel);

void hal_entry(void)
{
    ssp_err_t err;
    uint32_t const * p_data;

    g_next_bit = false;
    g_panel_channel_change = -1;   ///< No change required yet

    g_ioport.p_api->pinWrite(g_led_matrix_pins.oe, IOPORT_LEVEL_HIGH);  //Disable output enable

    R_PMISC->PWPR = 0;      ///< Clear BOWI bit - writing to PFSWE bit enabled
    R_PMISC->PWPR = 0x40;   ///< Set PFSWE bit - writing to PFS register enabled

    /** Setup IRQ on switches. */
    external_irq_cfg_t tmp_cfg = *g_switch_red_1.p_cfg;
    tmp_cfg.p_context = &g_switch_contexts[0];
    g_switch_red_1.p_api->open(g_switch_red_1.p_ctrl, &tmp_cfg);
    /** Must change callback manually due to issue in XML */
    tmp_cfg = *g_switch_red_2.p_cfg;
    tmp_cfg.p_context = &g_switch_contexts[1];
    tmp_cfg.p_callback = g_switch_red_1.p_cfg->p_callback;
    g_switch_red_2.p_api->open(g_switch_red_2.p_ctrl, &tmp_cfg);
    tmp_cfg = *g_switch_green_1.p_cfg;
    tmp_cfg.p_context = &g_switch_contexts[2];
    tmp_cfg.p_callback = g_switch_red_1.p_cfg->p_callback;
    g_switch_green_1.p_api->open(g_switch_green_1.p_ctrl, &tmp_cfg);
    tmp_cfg = *g_switch_green_2.p_cfg;
    tmp_cfg.p_context = &g_switch_contexts[3];
    tmp_cfg.p_callback = g_switch_red_1.p_cfg->p_callback;
    g_switch_green_2.p_api->open(g_switch_green_2.p_ctrl, &tmp_cfg);

    /** Create DTC buffer */
    led_matrix_frame_t frame;
    frame.color_depth = 8;
    frame.height = LED_MATRIX_COLS;
    frame.width = LED_MATRIX_ROWS;
    frame.panel_number = 0;
    frame.p_data = g_images_panel_0[0][0].p_data;
    led_matrix_parse(&frame , &g_dtc_buffer[0][0], &g_transparencies[0]);
    /** Prewrite 2nd panel */
    frame.panel_number = 1;
    frame.p_data = g_images_panel_1[0][0].p_data;
    led_matrix_parse(&frame , &g_dtc_buffer[0][0], &g_pixel_white[0]);
    //led_matrix_parse(&frame , &g_dtc_buffer[1][0], &g_pixel_white[0]);
    frame.panel_number = 0;
    frame.p_data = g_images_panel_0[0][0].p_data;

    p_data = &g_dtc_buffer[0][0];

    g_transfer.p_cfg->p_info->p_src = &p_data[0];
    g_dtc_length = sizeof(g_dtc_buffer[0])/sizeof(g_dtc_buffer[0][0]);
    g_transfer.p_cfg->p_info->length = g_dtc_length;
    g_transfer.p_api->open(g_transfer.p_ctrl, g_transfer.p_cfg);

    /* Setup timer that will drive one write (2 rows) */
    err = g_timer_clk.p_api->open(g_timer_clk.p_ctrl, g_timer_clk.p_cfg);
    /** Set GTIOR to have high output, toggle on Compare match */
    R_GPTA0->GTIOR_b.GTIOA = 0x19;
    //R_GPTA0->GTIOR_b.GTIOA = 0x06;
    /* Set GPT0 to be started/reset off of ELC event. */
    R_GPTA0->GTSSR_b.SSELCA = 1; ///< Start on GPT A event
    R_GPTA0->GTCSR_b.CSELCA = 1; ///< Clear on GPT A event
    R_GPTA0->GTPSR_b.PSELCB = 1; ///< Stop on GPT B event
    /* Have first compare match work. */
    R_GPTA0->GTCCRA = R_GPTA0->GTCCRC;

    NVIC_ClearPendingIRQ(GPT0_CAPTURE_COMPARE_A_IRQn);
    NVIC_EnableIRQ(GPT0_CAPTURE_COMPARE_A_IRQn);

    /* Setup timer that will control writing of color bits. */
    err |= g_timer_bit.p_api->open(g_timer_bit.p_ctrl, g_timer_bit.p_cfg);

    /** Get compare match time for bit clock to turn off clock timer */
    timer_info_t timer_info;
    g_timer_clk.p_api->infoGet(g_timer_clk.p_ctrl, &timer_info);

    /* Compare match will always be the same (after n clocks to panel) */
    R_GPTA1->GTCCRA = timer_info.period_counts * ((LED_MATRIX_COLS * LED_MATRIX_PANELS) + LED_MATRIX_DUMMY_COLS);
    R_GPTA1->GTCCRC = timer_info.period_counts * ((LED_MATRIX_COLS * LED_MATRIX_PANELS) + LED_MATRIX_DUMMY_COLS);
    R_GPTA1->GTCCRB = timer_info.period_counts * ((LED_MATRIX_COLS * LED_MATRIX_PANELS) + LED_MATRIX_DUMMY_COLS);
    R_GPTA1->GTCCRE = timer_info.period_counts * ((LED_MATRIX_COLS * LED_MATRIX_PANELS) + LED_MATRIX_DUMMY_COLS);
    R_GPTA1->GTIOR_b.GTIOB = 0x1B;

    /** Get lowest bit time and calculate others */
    g_timer_bit.p_api->infoGet(g_timer_bit.p_ctrl, &timer_info);

    g_bit_timer_values[0] = timer_info.period_counts;

    for (uint32_t i = 1; i < LED_MATRIX_COLOR_DEPTH; i++)
    {
        g_bit_timer_values[i] = g_bit_timer_values[0] << i;
    }

    /* Setup DTC for GPT periods. */
    g_transfer_gpt_period.p_cfg->p_info->p_src = &g_bit_timer_values[0];
    g_transfer_gpt_period.p_api->open(g_transfer_gpt_period.p_ctrl, g_transfer_gpt_period.p_cfg);

    /* Bit timer will start, reset, and stop clock timer */
    /* GPTA will be used to start/reset clock timer. */
    g_elc.p_api->linkSet(ELC_PERIPHERAL_GPT_A, ELC_EVENT_GPT1_COUNTER_OVERFLOW);
    /* GPTB will be used to stop clock timer. */
    g_elc.p_api->linkSet(ELC_PERIPHERAL_GPT_B, ELC_EVENT_GPT1_CAPTURE_COMPARE_B);
    /* Setup event that will actually write data to the port. */
    g_elc.p_api->linkSet(ELC_PERIPHERAL_IOPORT3, ELC_EVENT_GPT0_CAPTURE_COMPARE_A);
    g_elc.p_api->enable();

    g_transfer.p_api->enable(g_transfer.p_ctrl);    /* Enable DTC */

    /* Start bit timer */
    g_timer_bit.p_api->start(g_timer_bit.p_ctrl);

    /* Start graphic timer */
    g_counter = 0;
    g_change_graphic = false;
    g_change_dtc_src = false;
    g_timer_graphic.p_api->open(g_timer_graphic.p_ctrl, g_timer_graphic.p_cfg);

    g_panel_info[0].current_frame = 0;
    g_panel_info[0].current_image = 0;
    g_panel_info[0].current_loop = 0;
    g_panel_info[0].current_transparency = 0;
    g_panel_info[0].transparency = g_transparencies[0];
    g_panel_info[0].images = &g_images_panel_0[0][0];
    g_panel_info[0].transparencies = &g_transparencies[0];
    g_panel_info[0].transparencies_count = sizeof(g_transparencies)/sizeof(g_transparencies[0]);
    g_panel_info[0].updates_per_image = UPDATES_PER_IMAGE;

    g_panel_info[1].current_frame = 0;
    g_panel_info[1].current_image = 0;
    g_panel_info[1].current_loop = 0;
    g_panel_info[1].current_transparency = 0;
    g_panel_info[1].transparency = g_transparencies[0];
    g_panel_info[1].images = &g_images_panel_1[0][0];
    g_panel_info[1].transparencies = &g_transparencies[0];
    g_panel_info[1].transparencies_count = sizeof(g_transparencies)/sizeof(g_transparencies[0]);
    g_panel_info[1].updates_per_image = UPDATES_PER_IMAGE/4;


    led_matrix_sprite_t letter =
    {
        .x = 0,
        .y = 0,
        .width = 8,
        .height = 13,
        .p_data = g_led_matrix_letters[1]
    };

    led_matrix_pixel_t letter_color =
    {
        .r = 0xFF,
        .g = 0xFF,
        .b = 0xFF,
        .a = 0xFF
    };

    //led_matrix_add_sprite(&g_led_matrix_data_font[0], &letter, &letter_color);

    while (1)
    {
        led_matrix_panel_info_t * panel;

        if ((g_change_graphic == true) && (g_change_dtc_src == true))
        {
            uint32_t local_counter = g_counter + 1;

            g_change_graphic = false;
            g_change_dtc_src = false;

            /** Check to see if button has been pressed. */
            if (g_panel_channel_change >= 0)
            {
                /** Update channel being shown. */
                g_panel_info[0].images = &g_images_panel_0[g_panel_channel_change][0];
                g_panel_info[0].current_frame = 0;
                g_panel_info[0].current_image = 0;
                g_panel_info[0].current_loop = 0;
                g_panel_info[1].images = &g_images_panel_1[g_panel_channel_change][0];
                g_panel_info[1].current_frame = 0;
                g_panel_info[1].current_image = 0;
                g_panel_info[1].current_loop = 0;
                g_counter = 0;

                g_panel_channel_change = -1;
            }

            for (uint32_t i = 0; i < LED_MATRIX_PANELS; i++)
            {
                panel = &g_panel_info[i];

                /** Want to loop faster through animations. */
                if (panel->images[panel->current_image].frames > 1)
                {
                    panel->current_frame++;

                    if (panel->current_frame >= panel->images[panel->current_image].frames)
                    {
                        panel->current_frame = 0;
                    }
                }

                /** Move to next image, or set of images. */
                if ((local_counter % panel->updates_per_image) == 0)
                {
                    panel->current_loop++;

                    if (panel->current_loop >= panel->images[panel->current_image].loops)
                    {
                        panel->current_image++;

                        /** Check for empty image to signal end */
                        if (panel->images[panel->current_image].frames == 0)
                        {
                            panel->current_image = 0;
                        }

                        panel->current_frame = 0;
                        panel->current_loop = 0;
                    }
                }

                frame.p_data = &panel->images[panel->current_image].p_data[panel->current_frame];

                if (panel->current_transparency >= panel->transparencies_count)
                {
                    panel->current_transparency = 0;
                }

                /** Adjust current transparency to desired */
                if (true == led_matrix_pixel_merge_to(&panel->transparency, &panel->transparencies[panel->current_transparency]))
                {
                    /** Move to next */
                    panel->current_transparency++;
                }

                frame.panel_number = i;
                led_matrix_parse(&frame , &g_dtc_buffer[local_counter%2][0], &panel->transparency);
            }

            g_counter++;
        }
    }
}

void gpt0_capture_compare_int_a_isr (void)
{
    NVIC_ClearPendingIRQ(GPT0_CAPTURE_COMPARE_A_IRQn);
    R_BSP_IrqStatusClear(GPT0_CAPTURE_COMPARE_A_IRQn);

    g_transfer.p_api->reset(g_transfer.p_ctrl,
                            &g_dtc_buffer[g_counter%2][0],
                            &R_IOPORT3->PCNTR4,
                            g_dtc_length);

    g_change_dtc_src = true;
}


void timer_change_graphic_callback (timer_callback_args_t * p_args)
{
    g_change_graphic = true;
}

void switch_callback (external_irq_callback_args_t * p_args)
{
    ///< Handle switch press
    //g_change_graphic = true;
    /** Get channel to change to from context */
    g_panel_channel_change = ((switch_context_t *)p_args->p_context)->channel;
}

bool led_matrix_pixel_merge_to (led_matrix_pixel_t * p_pixel, led_matrix_pixel_t * p_to)
{
    bool err = true;

    if (p_pixel->r != p_to->r)
    {
        /* Not the same, start merge */
        if (p_pixel->r > p_to->r)
        {
            p_pixel->r--;
        }
        else
        {
            p_pixel->r++;
        }

        err = false;
    }

    if (p_pixel->g != p_to->g)
    {
        /* Not the same, start merge */
        if (p_pixel->g > p_to->g)
        {
            p_pixel->g--;
        }
        else
        {
            p_pixel->g++;
        }

        err = false;
    }

    if (p_pixel->b != p_to->b)
    {
        /* Not the same, start merge */
        if (p_pixel->b > p_to->b)
        {
            p_pixel->b--;
        }
        else
        {
            p_pixel->b++;
        }

        err = false;
    }

    if (p_pixel->a != p_to->a)
    {
        /* Not the same, start merge */
        if (p_pixel->a > p_to->a)
        {
            p_pixel->a--;
        }
        else
        {
            p_pixel->a++;
        }

        err = false;
    }

    return err;
}

void led_matrix_add_sprite (uint32_t * p_frame, led_matrix_sprite_t * p_sprite, led_matrix_pixel_t * p_pixel)
{
    uint32_t * p_cur_pixel;

    uint32_t x = p_sprite->x;
    uint32_t y = p_sprite->y;

    for (uint32_t col = 0; col < p_sprite->height; col++)
    {
        p_cur_pixel = &p_frame[(p_sprite->y * LED_MATRIX_COLS) + p_sprite->x];
        for (uint32_t row = 0; row < p_sprite->width; row++)
        {
            if ((p_sprite->p_data[col] >> row) & 1)
            {
                *p_cur_pixel = (uint32_t)p_pixel;
            }
        }
    }
}

