/***********************************************************************************************************************
 * Copyright [2020] Renesas Electronics Corporation and/or its affiliates.  All Rights Reserved.
 *
 * This software and documentation are supplied by Renesas Electronics America Inc. and may only be used with products
 * of Renesas Electronics Corp. and its affiliates ("Renesas").  No other uses are authorized.  Renesas products are
 * sold pursuant to Renesas terms and conditions of sale.  Purchasers are solely responsible for the selection and use
 * of Renesas products and Renesas assumes no liability.  No license, express or implied, to any intellectual property
 * right is granted by Renesas. This software is protected under all applicable laws, including copyright laws. Renesas
 * reserves the right to change or discontinue this software and/or this documentation. THE SOFTWARE AND DOCUMENTATION
 * IS DELIVERED TO YOU "AS IS," AND RENESAS MAKES NO REPRESENTATIONS OR WARRANTIES, AND TO THE FULLEST EXTENT
 * PERMISSIBLE UNDER APPLICABLE LAW, DISCLAIMS ALL WARRANTIES, WHETHER EXPLICITLY OR IMPLICITLY, INCLUDING WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NONINFRINGEMENT, WITH RESPECT TO THE SOFTWARE OR
 * DOCUMENTATION.  RENESAS SHALL HAVE NO LIABILITY ARISING OUT OF ANY SECURITY VULNERABILITY OR BREACH.  TO THE MAXIMUM
 * EXTENT PERMITTED BY LAW, IN NO EVENT WILL RENESAS BE LIABLE TO YOU IN CONNECTION WITH THE SOFTWARE OR DOCUMENTATION
 * (OR ANY PERSON OR ENTITY CLAIMING RIGHTS DERIVED FROM YOU) FOR ANY LOSS, DAMAGES, OR CLAIMS WHATSOEVER, INCLUDING,
 * WITHOUT LIMITATION, ANY DIRECT, CONSEQUENTIAL, SPECIAL, INDIRECT, PUNITIVE, OR INCIDENTAL DAMAGES; ANY LOST PROFITS,
 * OTHER ECONOMIC DAMAGE, PROPERTY DAMAGE, OR PERSONAL INJURY; AND EVEN IF RENESAS HAS BEEN ADVISED OF THE POSSIBILITY
 * OF SUCH LOSS, DAMAGES, CLAIMS OR COSTS.
 **********************************************************************************************************************/

#include "hal_data.h"

#include "r_dtc.h"

#include "led_matrix.h"
#include "led_matrix_images.h"
#include "led_matrix_channels.h"

#define GRAPHIC_PTR     g_led_matrix_data_raw_lightning
#define UPDATES_PER_IMAGE       (20)

#define HANDLE_ERROR(x) if (FSP_SUCCESS != err) { __BKPT(0); }

volatile int32_t g_panel_channel_change;
extern const led_matrix_images_t * g_images_panel_0[];

uint32_t g_led_matrix_data_font[1][1024];

volatile uint32_t g_images_index;

/** Buffer for DTC data */
/** TODO why is there an extra "column" of data on the end? */
/* This creates the 6240 that you see in the DTC length */
volatile uint32_t g_dtc_buffer[LED_MATRIX_PANELS][((LED_MATRIX_ROWS/LED_MATRIX_ROWS_AT_ONCE) * LED_MATRIX_COLS * LED_MATRIX_COLOR_DEPTH * LED_MATRIX_PANELS) + ((LED_MATRIX_ROWS/LED_MATRIX_ROWS_AT_ONCE) * LED_MATRIX_COLOR_DEPTH)];

const led_matrix_pins g_led_matrix_pins =
{
    .r1  = IOPORT_PORT_01_PIN_00,
    .g1  = IOPORT_PORT_01_PIN_01,
    .b1  = IOPORT_PORT_01_PIN_02,
    .r2  = IOPORT_PORT_01_PIN_03,
    .g2  = IOPORT_PORT_01_PIN_04,
    .b2  = IOPORT_PORT_01_PIN_05,
    .a   = IOPORT_PORT_01_PIN_06,
    .b   = IOPORT_PORT_01_PIN_07,
    .c   = IOPORT_PORT_01_PIN_11,
    .d   = IOPORT_PORT_01_PIN_12,
    .clk = IOPORT_PORT_04_PIN_15, //GPT output pin for CLK
    .lat = IOPORT_PORT_01_PIN_13,
    .oe  = IOPORT_PORT_01_PIN_14,
};

const led_matrix_pixel_t g_transparencies[] =
{
    { ///< Bright yellow
        .r = 255,
        .g = 245,
        .b = 100,
        .a = 255
    },
    { ///< Bright blue
        .r = 0x2d,
        .g = 0xb7,
        .b = 0xcb,
        .a = 255
    },
    { ///< Purple
        .r = 0x80,
        .g = 0x30,
        .b = 0x80,
        .a = 255
    },
    { ///< Red
        .r = 0xd5,
        .g = 0x10,
        .b = 0x09,
        .a = 255
    }
};

const led_matrix_pixel_t g_pixel_white[] =
{
    {
    .r = 0xC0,
    .g = 0xC0,
    .b = 0xC0,
    .a = 0xFF,
    }
};

const led_matrix_pixel_t g_pixel_black[] =
{
    {
    .r = 0,
    .g = 0,
    .b = 0,
    .a = 0xFF,
    }
};

typedef struct st_switch_context
{
    led_matrix_channel_t channel;
} switch_context_t;

#if 0
static const switch_context_t g_switch_contexts[] =
{
    { .channel = LED_MATRIX_CHANNEL_ALL },
    { .channel = LED_MATRIX_CHANNEL_DISNEY},
    { .channel = LED_MATRIX_CHANNEL_HALLOWEEN},
    { .channel = LED_MATRIX_CHANNEL_NCSU},
};
#endif

led_matrix_panel_info_t g_panel_info[LED_MATRIX_PANELS];

/* TODO what is g_tmp for? */
//ioport_port_pin_t g_tmp = IOPORT_PORT_05_PIN_11;
volatile bool g_next_bit;
volatile uint32_t g_clk_count = 0;
volatile bool     g_stop_timer;
uint32_t g_bit_timer_values[LED_MATRIX_COLOR_DEPTH];
volatile bool g_change_graphic;
volatile bool g_change_dtc_src;
uint16_t  g_dtc_length;
volatile uint32_t g_counter;

bool led_matrix_pixel_merge_to(led_matrix_pixel_t * p_pixel, led_matrix_pixel_t * p_to);
void led_matrix_add_sprite(uint32_t * p_frame, led_matrix_sprite_t * p_sprite, led_matrix_pixel_t * p_pixel);

void R_BSP_WarmStart(bsp_warm_start_event_t event);

extern bsp_leds_t g_bsp_leds;

/* This timer will be used for outputting each bit of color depth */
void timer_bit_callback(timer_callback_args_t * p_args);
gpt_instance_ctrl_t g_timer_bit_ctrl;
const gpt_extended_cfg_t g_timer_bit_extend =
{ .gtioca =
        { .output_enabled = false, .stop_level = GPT_PIN_LEVEL_HIGH },
          .gtiocb =
          { .output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW },
          .shortest_pwm_signal = GPT_SHORTEST_LEVEL_ON, .start_source = GPT_SOURCE_NONE, .stop_source =
                GPT_SOURCE_NONE,
          .clear_source = GPT_SOURCE_NONE, .count_up_source = GPT_SOURCE_NONE, .count_down_source =
                  GPT_SOURCE_NONE,
          .capture_a_source = GPT_SOURCE_NONE, .capture_b_source = GPT_SOURCE_NONE, .capture_a_ipl = BSP_IRQ_DISABLED, .capture_b_ipl =
                  (BSP_IRQ_DISABLED),
          .capture_a_irq = FSP_INVALID_VECTOR,
          .capture_b_irq = FSP_INVALID_VECTOR,
          .capture_filter_gtioca = GPT_CAPTURE_FILTER_NONE,
          .capture_filter_gtiocb = GPT_CAPTURE_FILTER_NONE,
          .p_pwm_cfg = NULL,
        };

timer_cfg_t g_timer_bit_cfg =
{
    .channel = 1,
    .cycle_end_ipl = 2,
    .cycle_end_irq = VECTOR_NUMBER_GPT1_COUNTER_OVERFLOW,
    .mode = TIMER_MODE_PERIODIC,
    .p_callback = timer_bit_callback,
    .p_context = NULL,
    .period_counts = 0, //Will set at runtime based on CLK GPT
    .source_div = 0, //Will set at runtime based on CLK GPT
    .p_extend = &g_timer_bit_extend,
};

/* This DTC instance will update GPT period for each color depth bit */
dtc_instance_ctrl_t g_dtc_timer_period_ctrl;
transfer_info_t g_dtc_timer_period_info =
{ .dest_addr_mode = TRANSFER_ADDR_MODE_FIXED,
  .repeat_area = TRANSFER_REPEAT_AREA_SOURCE,
  .irq = TRANSFER_IRQ_END,
  .chain_mode = TRANSFER_CHAIN_MODE_DISABLED,
  .src_addr_mode = TRANSFER_ADDR_MODE_INCREMENTED,
  .size = TRANSFER_SIZE_4_BYTE,
  .mode = TRANSFER_MODE_REPEAT,
  .p_dest = (void*) &R_GPT1->GTPR,
  .p_src = (void const*) &g_bit_timer_values[0],
  .num_blocks = 0,
  .length = LED_MATRIX_COLOR_DEPTH, };
const dtc_extended_cfg_t g_dtc_timer_period_cfg_extend =
{ .activation_source = VECTOR_NUMBER_GPT1_COUNTER_OVERFLOW, };
const transfer_cfg_t g_dtc_timer_period_cfg =
{ .p_info = &g_dtc_timer_period_info, .p_extend = &g_dtc_timer_period_cfg_extend, };

/* Instance structure to use this module. */
const transfer_instance_t g_dtc_timer_period =
{ .p_ctrl = &g_dtc_timer_period_ctrl, .p_cfg = &g_dtc_timer_period_cfg, .p_api = &g_transfer_on_dtc };

extern const elc_cfg_t g_elc_cfg;

/*******************************************************************************************************************//**
 * @brief  Blinky example application
 *
 * Blinks all leds at a rate of 1 second using the software delay function provided by the BSP.
 *
 **********************************************************************************************************************/
void hal_entry (void)
{
    fsp_err_t err;
    volatile uint32_t * volatile p_data;

    g_next_bit = false;
    g_panel_channel_change = -1;   ///< No change required yet

    err = R_ELC_Open(&g_elc_ctrl, &g_elc_cfg);

    HANDLE_ERROR(err);

    err = R_ELC_Enable(&g_elc_ctrl);

    HANDLE_ERROR(err);

    R_BSP_PinWrite(g_led_matrix_pins.oe, BSP_IO_LEVEL_HIGH); //Disable output enable

    R_BSP_PinAccessEnable();

    /** Setup IRQ on switches. */
#if 0
    external_irq_cfg_t tmp_cfg = *g_switch_red_1.p_cfg;
    tmp_cfg.p_context = &g_switch_contexts[0];
    g_switch_red_1.p_api->open(g_switch_red_1.p_ctrl, &tmp_cfg);
    /** Must change callback manually due to issue in XML */
    tmp_cfg = *g_switch_red_2.p_cfg;
    tmp_cfg.p_context = &g_switch_contexts[1];
    tmp_cfg.p_callback = g_switch_red_1.p_cfg->p_callback;
    g_switch_red_2.p_api->open(g_switch_red_2.p_ctrl, &tmp_cfg);
    tmp_cfg = *g_switch_green_1.p_cfg;
    tmp_cfg.p_context = &g_switch_contexts[2];
    tmp_cfg.p_callback = g_switch_red_1.p_cfg->p_callback;
    g_switch_green_1.p_api->open(g_switch_green_1.p_ctrl, &tmp_cfg);
    tmp_cfg = *g_switch_green_2.p_cfg;
    tmp_cfg.p_context = &g_switch_contexts[3];
    tmp_cfg.p_callback = g_switch_red_1.p_cfg->p_callback;
    g_switch_green_2.p_api->open(g_switch_green_2.p_ctrl, &tmp_cfg);
#endif

    /** Create DTC buffer */
    led_matrix_frame_t frame;
    frame.color_depth = 8;
    frame.height = LED_MATRIX_COLS;
    frame.width = LED_MATRIX_ROWS;
    frame.panel_number = 0;
    frame.p_data = (led_matrix_pixel_t *) g_images_panel_0[0][0].p_data;
    led_matrix_parse(&frame , (led_matrix_panel_frame_t *)&g_dtc_buffer[0][0], &g_transparencies[0]);
#if 0 //TODO No 2nd panel for the moment
    /** Prewrite 2nd panel */
    frame.panel_number = 1;
    frame.p_data = g_images_panel_1[0][0].p_data;
    led_matrix_parse(&frame , &g_dtc_buffer[0][0], &g_pixel_white[0]);
    //led_matrix_parse(&frame , &g_dtc_buffer[1][0], &g_pixel_white[0]);
    frame.panel_number = 0;
    frame.p_data = g_images_panel_0[0][0].p_data;
#endif

    p_data = &g_dtc_buffer[0][0];

#if 1
    /* TODO need to setup DTC with defaults */
    g_led_transfer_cfg.p_info->p_src = (void const * volatile) &p_data[0];
    g_led_transfer_cfg.p_info->p_dest = (void * volatile ) &R_PORT1->PCNTR4;
    g_dtc_length = sizeof(g_dtc_buffer[0])/sizeof(g_dtc_buffer[0][0]);
    g_led_transfer_cfg.p_info->length = g_dtc_length;
    err = R_DTC_Open(&g_led_transfer_ctrl, &g_led_transfer_cfg);

    HANDLE_ERROR(err);

    err = R_GPT_Open(&g_timer_clk_ctrl, &g_timer_clk_cfg);
    HANDLE_ERROR(err);
    err = R_GPT_Enable(&g_timer_clk_ctrl);
    HANDLE_ERROR(err);

    /* Set period of timer that will control each bit of color depth */
    timer_info_t clk_info;
    err = R_GPT_InfoGet(&g_timer_clk_ctrl, &clk_info);
    HANDLE_ERROR(err);

    /* This is time per color depth bit. The extra period_counts at the end is to have some difference between this
       period which will reset the CLK timer and the compare match which will stop the CLK timer. */
    g_timer_bit_cfg.period_counts = (clk_info.period_counts * ((LED_MATRIX_COLS * LED_MATRIX_PANELS) + LED_MATRIX_DUMMY_COLS)) + clk_info.period_counts;
    g_timer_bit_cfg.source_div = g_timer_clk_cfg.source_div;

    err = R_GPT_Open(&g_timer_bit_ctrl, &g_timer_bit_cfg);
    HANDLE_ERROR(err);

    /* Compare match will always be the same (after n clocks to panel) */
    R_GPT1->GTCCR[0] = clk_info.period_counts * ((LED_MATRIX_COLS * LED_MATRIX_PANELS) + LED_MATRIX_DUMMY_COLS);
    R_GPT1->GTCCR[2] = clk_info.period_counts * ((LED_MATRIX_COLS * LED_MATRIX_PANELS) + LED_MATRIX_DUMMY_COLS);

    /** Get lowest bit time and calculate others */
    err = R_GPT_InfoGet(&g_timer_bit_ctrl, &clk_info);

    g_bit_timer_values[0] = clk_info.period_counts;

    for (uint32_t i = 1; i < LED_MATRIX_COLOR_DEPTH; i++)
    {
        g_bit_timer_values[i] = g_bit_timer_values[0] << i;
    }

    err = R_DTC_Open(&g_dtc_timer_period_ctrl, &g_dtc_timer_period_cfg);

    HANDLE_ERROR(err);

    /* Enable DTCs */
    err = R_DTC_Enable(&g_led_transfer_ctrl);
    HANDLE_ERROR(err);

    err = R_DTC_Enable(&g_dtc_timer_period_ctrl);
    HANDLE_ERROR(err);

    /* Start GPT1 which will control GPT0 */
    err = R_GPT_Start(&g_timer_bit_ctrl);
    HANDLE_ERROR(err);

#endif

    /* Define the units to be used with the software delay function */
    const bsp_delay_units_t bsp_delay_units = BSP_DELAY_UNITS_MILLISECONDS;

    /* Set the blink frequency (must be <= bsp_delay_units */
    const uint32_t freq_in_hz = 2;

    /* Calculate the delay in terms of bsp_delay_units */
    const uint32_t delay = bsp_delay_units / freq_in_hz;

    /* LED type structure */
    bsp_leds_t leds = g_bsp_leds;

    /* If this board has no LEDs then trap here */
    if (0 == leds.led_count)
    {
        while (1)
        {
            ;                          // There are no LEDs on this board
        }
    }

    /* Holds level to set for pins */
    bsp_io_level_t pin_level = BSP_IO_LEVEL_LOW;

    while (1)
    {
        /* Enable access to the PFS registers. If using r_ioport module then register protection is automatically
         * handled. This code uses BSP IO functions to show how it is used.
         */
        //R_BSP_PinAccessEnable();

        /* Update all board LEDs */
        /* Minus 1 bc LED is on data lines for display */
        for (uint32_t i = 0; i < (leds.led_count - 1); i++)
        {
            /* Get pin to toggle */
            uint32_t pin = leds.p_leds[i];

            /* Write to this pin */
            R_BSP_PinWrite((bsp_io_port_pin_t) pin, pin_level);
        }

        /* Protect PFS registers */
        //R_BSP_PinAccessDisable();

        /* Toggle level for next write */
        if (BSP_IO_LEVEL_LOW == pin_level)
        {
            pin_level = BSP_IO_LEVEL_HIGH;
        }
        else
        {
            pin_level = BSP_IO_LEVEL_LOW;
        }

        /* Delay */
        R_BSP_SoftwareDelay(delay, bsp_delay_units);
    }
}

/*******************************************************************************************************************//**
 * This function is called at various points during the startup process.  This implementation uses the event that is
 * called right before main() to set up the pins.
 *
 * @param[in]  event    Where at in the start up process the code is currently at
 **********************************************************************************************************************/
void R_BSP_WarmStart (bsp_warm_start_event_t event)
{
    if (BSP_WARM_START_RESET == event)
    {
#if BSP_FEATURE_FLASH_LP_VERSION != 0

        /* Enable reading from data flash. */
        R_FACI_LP->DFLCTL = 1U;

        /* Would normally have to wait tDSTOP(6us) for data flash recovery. Placing the enable here, before clock and
         * C runtime initialization, should negate the need for a delay since the initialization will typically take more than 6us. */
#endif
    }

    if (BSP_WARM_START_POST_C == event)
    {
        /* C runtime environment and system clocks are setup. */

        /* Configure pins. */
        R_IOPORT_Open(&g_ioport_ctrl, &g_bsp_pin_cfg);
    }
}

void led_clk_callback(timer_callback_args_t *p_args)
{
    /* All panels have been updated. Reset DTC for when GPT1 starts again. */
    fsp_err_t err = R_DTC_Reset(&g_led_transfer_ctrl, g_led_transfer_cfg.p_info->p_src, g_led_transfer_cfg.p_info->p_dest, g_led_transfer_cfg.p_info->length);

    HANDLE_ERROR(err);
}

void timer_bit_callback(timer_callback_args_t *p_args)
{
    __NOP();
}
